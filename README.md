chorner
========

C99 implementation of compensated Horner evaluation of polynomials,
real and complex, as described in [1], [2].

1. S. Graillat, N. Louvet, P. Langlois, _Compensated Horner scheme_,
Research Report 04, Équipe de recherche DALI, Laboratoire LP2A,
Université de Perpignan Via Domitia, France, July 2005.
[pdf](http://www-pequan.lip6.fr/~graillat/papers/rr2005-04.pdf)

2. Stef Graillat, Valérie Ménissier-Morain, _Compensated Horner
scheme in complex floating point arithmetic_, Proceedings of the
8th Conference on Real Numbers and Computers, Santiago de
Compostela, Spain, July 7-9, 2008, p.133-146
[pdf](http://www-pequan.lip6.fr/~graillat/papers/rnc08.pdf)

Unit tests pass with gcc and clang with `-Wall -O3 -std=c99`.
