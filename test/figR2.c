/*
  Reproduces Figure 2 of the report RR2005-04, the illustrated
  inequality (from Theorem 6, ibid.) used in our unit tests.
*/

#include <math.h>
#include <stdio.h>
#include <gmp.h>

#include "chorner.h"

#define RELATIVE_ERROR_UNIT 0x1p-53
#define GAMMA(n, u) (((n) * (u))/(1.0 - ((n)*(u))))

int main(void)
{
  double
    u = RELATIVE_ERROR_UNIT,
    p[6] = {1, -5, 10, -10, 5, -1};

  mpf_t mHc, meps, mP, mres;

  mpf_set_default_prec(256);
  mpf_inits(mHc, meps, mP, mres, NULL);

  for (int n = -1000 ; n <= 1000 ; n++)
    {
      double
	x = 1 - n * 1e-5,
	Hc = crhorner(p, 6, x);

      mpf_set_d(mHc, Hc);
      mpf_set_d(meps, 1-x);
      mpf_pow_ui(mP, meps, 5);
      mpf_sub(mres, mHc, mP);

      double
	res = fabs(mpf_get_d(mres)),
	P = mpf_get_d(mP),
	cond = pow(fabs((1+x)/(1-x)), 5),
	bound = fabs(P * (u + pow(GAMMA(10, u), 2)*cond));

      printf("%e %e %e\n", x, res, bound);
    }
}
