set termopt enhanced
set format y "10^{%L}"
set logscale y
set xlabel "Polynomial argument x"
set ylabel "Absolute error"

plot \
     "figR2.dat" using 1:2 \
     with points pointtype 7 pointsize 1 title "Compensated Horner", \
     "figR2.dat" using 1:3 \
     with lines title "Theorem 6"
