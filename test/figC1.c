/*
  Reproduces Figure 1 of the report RR2005-04, the illustrated
  inequality (from Theorem 6, ibid.) used in our unit tests.
*/

#include <math.h>
#include <complex.h>
#include <stdio.h>
#include <string.h>
#include <mpc.h>

#include "chorner.h"

#define RELATIVE_ERROR_UNIT 0x1p-53
#define GAMMA(n, u) (((n) * (u))/(1.0 - ((n)*(u))))

/*
  when passed a polynomial p(x) order n-1, modifies in place
  to produce the polynomial (x-(1+i))p(x), the caller should
  ensure that the array has enough room to hold the result
  since this is not checked here
*/

static void next_poly(double complex *p, size_t n)
{
  double complex q[n];

  memcpy(q, p, n * sizeof(double complex));
  p[n] = 0.0;

  for (size_t i = 0 ; i < n ; i++) p[i] = q[i] * (-1 - I);
  for (size_t i = 0 ; i < n ; i++) p[i+1] += q[i];
}

#define PREC 256

int main(void)
{
  double
    u = RELATIVE_ERROR_UNIT;
  double complex
    x = 1.333 + 1.333 * I,
    p[43] = {2*I, -2-2*I, 1};

  mpc_t mHc, meps, mP, mres;

  mpc_init2(mHc, PREC);
  mpc_init2(meps, PREC);
  mpc_init2(mP, PREC);
  mpc_init2(mres, PREC);

  for (size_t n = 3; n < 43 ; n++)
    {
      next_poly(p, n);

      double complex Hc = cchorner(p, n+1, x);

      mpc_set_dc(mHc, Hc, MPC_RNDNU);
      mpc_set_dc(meps, x-1-I, MPC_RNDNU);
      mpc_pow_ui(mP, meps, n, MPC_RNDNU);
      mpc_sub(mres, mHc, mP, MPC_RNDNU);

      double
        res = cabs(mpc_get_dc(mres, MPC_RNDNU)),
        P = cabs(mpc_get_dc(mP, MPC_RNDNU)),
        cond = pow(cabs((1+I+x)/(1+I-x)), n),
        bound = fabs(u + pow(GAMMA(2*n, u), 2)*cond);

      printf("%e %e %e\n",
	     cond, res/P, bound);
    }

  mpc_clear(mHc);
  mpc_clear(meps);
  mpc_clear(mP);
  mpc_clear(mres);
}
