/*
  tests_cchorner.h

  Copyright (c) J.J. Green 2016
*/

#ifndef TESTS_CCHORNER_H
#define TESTS_CCHORNER_H

#include <CUnit/CUnit.h>

extern CU_TestInfo tests_cchorner[];

extern void test_cchorner_empty(void);
extern void test_cchorner_constant(void);
extern void test_cchorner_linear1(void);
extern void test_cchorner_linear2(void);
extern void test_cchorner_figure1(void);

#endif
